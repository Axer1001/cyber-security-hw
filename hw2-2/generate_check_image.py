from PIL import Image
import numpy as np

image_pix = []
k = 0
for i in range(120):
    row = []
    for j in range(120):
        row.append([(k + 30) % 256 for i in range(3)])
        k += 30
    image_pix.append(row)

image_pix = np.array(image_pix)

im = Image.fromarray(image_pix.astype('uint8'), 'RGB')
im.save('cvs.png')
