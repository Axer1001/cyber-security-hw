from PIL import Image
import numpy as np
from math import floor
from colormap import rgb2hex

T = 125

def arnold_shuffle(image_pix):
    k = np.array([[1,1], [1,2]])

    h, w = image_pix.shape
    new_matrix = image_pix.copy()

    for i in range(h):
        for j in range(w):
            x, y = (k @ np.array([[i],[j]])) % h
            x = x[0]
            y = y[0]

            new_matrix[x][y] = image_pix[i][j]

    return new_matrix


def arnold_unshuffle(image_pix):
    k_inv = np.array([[2,-1],[-1,1]])

    h, w = image_pix.shape
    new_matrix = image_pix.copy()

    for i in range(h):
        for j in range(w):
            x, y = (k_inv @ np.array([[i],[j]])) % h
            x = x[0]
            y = y[0]
            new_matrix[x][y] = image_pix[i][j]

    return new_matrix


def process_layers(cert_layer, v_layer):
    
    def layer_to_string(layer):
        return "".join([format(n, '08b') for n in layer.flatten()])
    
    layer_bytes = layer_to_string(cert_layer)
    #print(layer_bytes)
    
    def process_block(block, byte):
        dcc = block.sum()

        clow = dcc // T * T + 0.25 * T if byte == '1' else (dcc // T - 1) * T + 0.75 * T
        chigh = (dcc // T + 1) * T + 0.25 * T if byte == '1' else dcc // T * T + 0.75 * T

        oc = clow if abs(clow - dcc) <= abs(chigh - dcc) else chigh

        new_block = block.copy()

        for i in range(4):
            for j in range(4):
                new_block[i,j] = int(block[i,j] + (oc - dcc) / 16) % 256
        
        return new_block

    k = 0

    w, h = v_layer.shape
    
    new_layer = v_layer.copy()

    for i in range(0, w, 4):
        for j in range(0, h, 4):
            if k == len(layer_bytes):
                break

            if v_layer[i:i+4, j:j+4].shape != (4,4):
                continue

            new_layer[i:i+4, j:j+4] = process_block(v_layer[i:i+4, j:j+4], layer_bytes[k])
            k += 1
    
    return new_layer


def deprocess_layer(v_layer, w, h):
    
    def string_to_layer(bytes_string):
        layer = np.array([int(bytes_string[i:i+8], 2) for i in range(0,len(bytes_string), 8)])
        layer = layer.reshape(h,w)
        return layer
    
    def block_to_byte(block):
        dcc_star = block.sum()

        if dcc_star % T < 0.5 * T:
            return '1'
        
        return '0'

    wv, hv = v_layer.shape

    extracted_bytes = ""

    for i in range(0, wv, 4):
        for j in range(0, hv, 4):
            if len(extracted_bytes) == w * h * 8:
                break

            if v_layer[i:i+4, j:j+4].shape != (4,4):
                continue

            extracted_bytes += block_to_byte(v_layer[i:i+4, j:j+4])

    return string_to_layer(extracted_bytes)

def calculate_stats(file1: str, file2:str):
    im1 = Image.open(file1)
    im2 = Image.open(file2)
    
    pix1 = im1.load()
    pix2 = im2.load()
    w, h = im1.size

    mse = 0
    for i in range(w):
        for j in range(h):
            r, g, b = pix1[i, j][:3]
            c1 = int(rgb2hex(r,g,b)[1:], 16)
            r, g, b = pix2[i, j][:3]
            c2 = int(rgb2hex(r,g,b)[1:], 16)
            mse += (c1 - c2) ** 2
    
    print('\n------------------')
    print(' [x] MSE: ', mse / (w * h))
    print('')


def main():
    mode = int(input('Enter mode[0=put/1=extract/2=stats]: '))
    if mode == 0:
        cert = Image.open('cvs.png')

        image_name = input('Enter image name: ')
        victim = Image.open(image_name)

        assert (cert.width * cert.height * 8) <= (victim.width * victim.height / 16), "Not enough space"

        r_cert, g_cert, b_cert, = cert.split()[:3]
        r_victim, g_victim, b_victim = victim.split()[:3]

        r_res = process_layers(
            arnold_shuffle(np.array(r_cert).T), np.array(r_victim).T
        )

        g_res = process_layers(
            arnold_shuffle(np.array(g_cert).T), np.array(g_victim).T
        )

        b_res = process_layers(
            arnold_shuffle(np.array(b_cert).T), np.array(b_victim).T
        )

        new_matrix = victim.load()
        w, h = victim.size

        for i in range(w):
            for j in range(h):
                new_matrix[i,j] = (r_res[i,j], g_res[i,j], b_res[i,j])
        
        victim.save('result.png')
    elif mode == 1:
        W, H = 80, 80
        image_name = input('Enter container image name: ')
        victim = Image.open(image_name)
        r_v, g_v, b_v = victim.split()[:3]

        r_cert = deprocess_layer(
            np.array(r_v).T,
            W, H
        )
        
        g_cert = deprocess_layer(
            np.array(g_v).T,
            W, H
        )

        b_cert = deprocess_layer(
            np.array(b_v).T,
            W, H
        )

        r_cert = arnold_unshuffle(r_cert)
        b_cert = arnold_unshuffle(b_cert)
        g_cert = arnold_unshuffle(g_cert)

        new_matrix = []

        for i in range(W):
            row = []
            for j in range(H):
                row.append( (r_cert[j,i], g_cert[j,i], b_cert[j,i]) )
            
            new_matrix.append(row)

        new_matrix = np.array(new_matrix)

        extracted = Image.fromarray(new_matrix.astype('uint8'), 'RGB')
        extracted.save('extracted.png')
    else:
        original_name = input('Enter original image name: ')
        image_name = input('Enter container image name: ')
        calculate_stats(image_name, original_name)


if __name__ == '__main__':
    main()