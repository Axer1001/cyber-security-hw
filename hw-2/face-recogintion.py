import cv2
import face_recognition

def authenticate():
    camera = cv2.VideoCapture(0)

    return_value, image = camera.read()
    cv2.imwrite('opencv.jpg', image)
    del(camera)

    known_image = face_recognition.load_image_file("user.jpg")
    unknown_image = face_recognition.load_image_file("opencv.jpg")

    user_encoding = face_recognition.face_encodings(known_image)[0]

    unknown_encoding = face_recognition.face_encodings(unknown_image)

    if not unknown_encoding:
        return False

    unknown_encoding = unknown_encoding[0]
    
    results = face_recognition.compare_faces([user_encoding], unknown_encoding)
    
    return results[0]


def main():
    print(' [x] Authetication needed', flush=True)

    for i in range(3, 0, -1):
        print(' [x] Attempting authentication in', i)
    
    print(' [x] Access granted' if authenticate() else ' [x] Access denied')

if __name__ == '__main__':
    main()