from hashlib import new
from logging import exception
from multiprocessing.sharedctypes import Value
from PIL import Image
from colormap import rgb2hex, hex2rgb


def embed(filename: str, message: str):
    q = 6

    im = Image.open(filename)
    pix = im.load()
    w, h = im.size
    
    k = 0
    bin_message = ''.join(format(ord(c), '08b') for c in message)
    
    if len(bin_message) + 32 > w * h:
        raise ValueError(' [ERROR] Message too big to fit in this image!')

    bin_message_len = format(len(bin_message), '032b')
    z = 0

    for i in range(w):
        for j in range(h):
            
            if k == len(bin_message):
                break

            r, g, b = pix[i, j]
            old_val = int(rgb2hex(r,g,b)[1:], 16)
            
            if z < len(bin_message_len):
                new_val = hex(q * (old_val // q) + q // 2 * int(bin_message_len[z]))
                z += 1
            else:
                new_val = hex(q * (old_val // q) + q // 2 * int(bin_message[k]))
                k += 1
            
            new_val = new_val[:2] + '0' * (8-len(new_val)) + new_val[2:]
            
            r, g, b = hex2rgb(new_val)
            pix[i, j] = (r,g,b)
    
    im.save('out.png')

    print(" [x] Information stored in out.png!")


def extract(filename:str):
    q = 6

    im = Image.open(filename)
    pix = im.load()
    w, h = im.size
    
    k = 0
    bin_message = ''
    
    bin_message_len = ''
    z = 0

    message_len = -1

    for i in range(w):
        for j in range(h):
            
            if k == message_len:
                break

            if z == 32:
                z += 1
                message_len = int(bin_message_len, 2)


            r, g, b = pix[i, j]
            val = int(rgb2hex(r,g,b)[1:], 16)

            c0 = q * (val // q)
            c1 = q * (val // q) + q // 2

            bit = '0' if abs(val -c0) < abs(val - c1) else '1'
            
            if z < 32:
                bin_message_len += bit
                z += 1
            else:
                bin_message += bit
                k += 1

    print(message_len)
    chars = []
    for i in range(0, min(message_len, w * h), 8):
        chars.append(bin_message[i:i+8])

    for i in range(0, len(chars)):
        try:
            chars[i] = chr(int(chars[i], 2))
        except:
            chars[i] = '_'
    res = ''.join(chars)
    print(' [x] Message: ', res)


def calculate_stats(file1: str, file2:str):
    im1 = Image.open(file1)
    im2 = Image.open(file2)
    
    pix1 = im1.load()
    pix2 = im2.load()
    w, h = im1.size

    mse = 0
    for i in range(w):
        for j in range(h):
            r, g, b = pix1[i, j]
            c1 = int(rgb2hex(r,g,b)[1:], 16)
            r, g, b = pix2[i, j]
            c2 = int(rgb2hex(r,g,b)[1:], 16)
            mse += (c1 - c2) ** 2
    
    print('\n------------------')
    print(' [x] MSE: ', mse / (w * h))
    print('')


def main():
    print('Choose one:')
    print(' [1] embed information in file')
    print(' [2] extract information from file')
    print(' [3] calculate statistics on files')
    option = int(input('Choice: '))
    
    if option < 1 or option > 3:
        print('Incorrect option')
        exit(1)
    
    file1 = input('Enter rgb image name: ')
    if option == 1:
        f = input('Enter message file name: ')
        with open(f, 'r') as f:
            message = f.read()
        embed(file1, message)
    
    if option == 2:
        extract(file1)

    if option == 3:
        file2 = input('Enter container file name: ')
        calculate_stats(file1, file2)


if __name__ == "__main__":
    main()