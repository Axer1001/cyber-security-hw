from PIL import Image

def crop_and_save(filename, delta=4):
    with Image.open(filename) as im:
        w, h = im.size

        im.crop((delta, delta, w, h)).save('cropped.jpeg')