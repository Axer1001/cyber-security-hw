import json
import numpy as np

with open('.fitted-stats.json') as f:
    data = json.load(f)

keys = np.array(list(data.keys()))

train_size = int(0.97 * len(keys))
test_size = len(keys) - train_size

test = np.random.choice(keys, test_size, replace=False)
train = np.setdiff1d(keys, test)

train = {k: data[k] for k in train}

with open('test.json', 'w') as f:
    json.dump(list(test), f, indent=4)

with open('train.json', 'w') as f:
    json.dump(train, f, indent=4)