import numpy as np
from crop import crop_and_save
import jpeglib

def histogram_vector(Y, CbCr):
    hist = {i: 0 for i in range(-255, 256)}

    def extract_coefs(layer):
        imsize = layer.shape
    
        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            
                            hist[layer[i, j, c, i1, j1]] += 1
    
    extract_coefs(Y)
    extract_coefs(CbCr)

    hist = np.array(list(hist.values()))
    hist = hist / np.linalg.norm(hist, ord=1)

    return hist


def individual_hists(Y, CbCr):
    hist_10 = {i: 0 for i in range(-255, 256)}
    hist_20 = {i: 0 for i in range(-255, 256)}
    hist_01 = {i: 0 for i in range(-255, 256)}
    hist_11 = {i: 0 for i in range(-255, 256)}
    hist_02 = {i: 0 for i in range(-255, 256)}
    
    
    def extract_coefs(layer):
        imsize = layer.shape
    
        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):
    
                        hist_10[layer[i, j, c, 1, 0]] += 1
                        hist_20[layer[i, j, c, 2, 0]] += 1
                        hist_01[layer[i, j, c, 0, 1]] += 1
                        hist_11[layer[i, j, c, 1, 1]] += 1
                        hist_02[layer[i, j, c, 0, 2]] += 1
    
    extract_coefs(Y)
    extract_coefs(CbCr)

    hists = [np.array(list(hist_10.values())),
    np.array(list(hist_20.values())),
    np.array(list(hist_01.values())),
    np.array(list(hist_11.values())),
    np.array(list(hist_02.values()))]
    
    for i in range(5):
        hists[i] = hists[i] / np.linalg.norm(hists[i], ord=1)

    return hists


def dual_hists(Y, CbCr):
    hists = {i: np.zeros((8,8)) for i in range(-5, 6)}

    def extract_coefs(layer):
        imsize = layer.shape
    
        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            if layer[i,j,c,i1,j1] >= -5 and layer[i,j,c,i1,j1] <= 5:
                                hists[layer[i,j,c,i1,j1]][i1][j1] += 1
    
    
    extract_coefs(Y)
    extract_coefs(CbCr)

    hists = list(hists.values())

    for i in range(len(hists)):
        hists[i] = hists[i] / np.linalg.norm(hists[i], ord=1)
    
    return hists


def calc_stats(filename):
    crop_and_save(filename)

    with jpeglib.JPEG(filename) as im:
        Y_orig, CbCr_orig, _ = im.read_dct()
    
    with jpeglib.JPEG('cropped.jpeg') as im:
        Y_crop, CbCr_crop, _ = im.read_dct()
    stats = []
    
    stats.append(np.linalg.norm(histogram_vector(Y_orig, CbCr_orig) - histogram_vector(Y_crop, CbCr_crop), ord=1))

    orig_indiv_hists = individual_hists(Y_orig, CbCr_orig)
    crop_indiv_hists = individual_hists(Y_crop, CbCr_crop)

    for i in range(5):
        stats.append(np.linalg.norm(
           orig_indiv_hists[i] - crop_indiv_hists[i],
           ord=1 
        ))
    
    orig_dual_hists = dual_hists(Y_orig, CbCr_orig)
    crop_dual_hists = dual_hists(Y_crop, CbCr_crop)

    for i in range(len(orig_dual_hists)):
        stats.append(np.linalg.norm(
            orig_dual_hists[i] - crop_dual_hists[i],
            ord=1
        ))




    return stats