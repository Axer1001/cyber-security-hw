import json
import os
from stats import calc_stats
import numpy as np
from tqdm import tqdm

def fit():
    stats = {}
    
    for file in tqdm(os.listdir('./dataset')):
        stats[file] = calc_stats('./dataset/' + file)
    
    return stats
    

def predict(file_to_predict, data, k=3):
    f = np.array(calc_stats(file_to_predict))

    items = list(data.items())

    items.sort(key= lambda x: np.linalg.norm(f - x[1], ord=1))

    p, n = 0, 0

    for i in range(k):
        if '_w' in items[0][0]:
            p += 1
        else:
            n += 1
    
    return 1.0 if p > n else 0.0 

def main():
    choice = int(input('Choose option:\n 1. Fit\n 2. Predict\n Choice: '))

    if choice == 1:
        results = fit()
        
        with open('.fitted-stats.json', 'w') as f:
            print(json.dumps(results, indent=4), file=f)
    
    elif choice == 2:
        try:
            f = open('train.json', 'r')
        except:
            raise ValueError('Need to fit before predicting!')
        
        data = json.load(f)
        
        for key, value in data.items():
            data[key] = np.array(value)
        
        
        file_to_predict = input('Enter filename: ')
        
        result = predict(file_to_predict, data)

        print(f'Predicted class: {result}')




if __name__ == '__main__':
    main()