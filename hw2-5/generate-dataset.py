import numpy as np
import os
from imageio import imread, imwrite
from encoders import lsb_encode, lsb_decode
from encoders import f3_encode, f3_decode
from encoders import f4_encode, f4_decode
from encoders import f5_encode, f5_decode
import jpeglib

def main():
    candidates = os.listdir('dataset')
    with_data  = np.random.choice(candidates, len(candidates) // 2, replace=False)

    method = np.random.randint(1, 5)
    
    with open('input') as f:
        msg = open('input').readline()
    
    for file in with_data:
        print(f'[x] Processing file {file}!')
        with jpeglib.JPEG("dataset/" + file) as im:
            Y_container, CbCr_container, _ = im.read_dct()
            
            Y_new, CbCr_new = Y_container.copy(), CbCr_container.copy() 
            
            if method == 1:
                Y_new, CbCr_new = lsb_encode(Y_new, CbCr_new, msg)
            elif method == 2:
                Y_new, CbCr_new = f3_encode(Y_new, CbCr_new, msg)
            elif method == 3:
                Y_new, CbCr_new = f4_encode(Y_new, CbCr_new, msg)
            elif method == 4:
                Y_new, CbCr_new = f5_encode(Y_new, CbCr_new, msg)
            
            new_name = file[:file.find('.jpeg')] + '_w.jpeg'
            im.write_dct('./dataset/' + new_name, Y_new, CbCr_new)        
        
        print(f'[x] Processed {file}!')

    for file in with_data:
        os.system('rm ./dataset/' + file)
    
if __name__ == '__main__':
    main()
