from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np

def get_AC(m1, m2) -> list:
    ac_coefs = []

    for i in range(m1.shape[0]):
        for j in range(m1.shape[1]):
            for k in range(m1.shape[2]):
                for i1 in range(m1.shape[3]):
                    for j1 in range(m1.shape[4]):
                        if i1 != 0 and j1 != 0:
                            ac_coefs.append(m1[i,j,k,i1,j1])

    for i in range(m2.shape[0]):
        for j in range(m2.shape[1]):
            for k in range(m2.shape[2]):
                for i1 in range(m2.shape[3]):
                    for j1 in range(m2.shape[4]):
                        if i1 != 0 and j1 != 0:
                            ac_coefs.append(m2[i,j,k,i1,j1])

    return ac_coefs


def plotter(Y_original, CbCr_original, Y_new, CbCr_new) -> None:
    old = get_AC(Y_original, CbCr_original)
    new = get_AC(Y_new, CbCr_new)
    
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

    sns.histplot(old, ax=ax1)

    sns.histplot(new, ax=ax2)

    def second_max(arr):
        u, c = np.unique(arr, return_counts=True)
        k = sorted(list(zip(u, c)), key=lambda x:x[1])[-2]
        return k[1]
    
    lim = max(second_max(old), second_max(new)) * 1.5
    
    ax1.set_ylim(0, lim)
    ax1.set_ylim(0, lim)

    plt.show()