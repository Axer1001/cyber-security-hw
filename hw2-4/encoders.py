import numpy as np

def encode_binary_message(msg: str) -> str:
    return ''.join(format(ord(c), '08b') for c in msg)

def decode_binary_message(bin_str: str) -> str:
    return ''.join(chr(int(bin_str[i:i+8],2)) for i in range(0, len(bin_str), 8))


def sign(n) -> int:
    return 1 if n > 0 else -1

def lsb_encode(Y, CbCr, msg):
    
    k = 0
    bin_message = encode_binary_message(msg)
    
    def insert_data(layer, bin_message, k) -> int:
        imsize = layer.shape
        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            if k >= len(bin_message):
                                continue

                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0 or coef == 1 or coef == -1:
                                continue

                            layer[i,j,c,i1,j1] = int(bin(coef)[:-1] + bin_message[k], 2)
                            k += 1
        
        return k

    k = insert_data(Y, bin_message, k)
    
    if k >= len(bin_message):
        return Y, CbCr
    
    insert_data(CbCr, bin_message, k)

    return Y, CbCr


def lsb_decode(Y, CbCr) -> str:
    bin_message = ''
    
    def extract_data(layer) -> str:
        imsize = layer.shape
        data = ''

        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            
                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0 or coef == 1 or coef == -1:
                                continue

                            data += bin(coef)[-1]
        return data

    bin_message += extract_data(Y) + extract_data(CbCr)

    return decode_binary_message(bin_message)


def f3_encode(Y, CbCr, msg):
    
    k = 0
    bin_message = encode_binary_message(msg)
    
    def insert_data(layer, bin_message, k) -> int:
        imsize = layer.shape
        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            if k >= len(bin_message):
                                continue

                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0:
                                continue
                            
                            if (abs(coef) % 2 == 1 and bin_message[k] == '0')\
                                 or (abs(coef) % 2 == 0 and bin_message[k] == '1'):
                                layer[i,j,c,i1,j1] = coef - 1
                        
                            if layer[i,j,c,i1,j1] != 0:
                                k += 1
        
        return k

    k = insert_data(Y, bin_message, k)
    
    if k >= len(bin_message):
        return Y, CbCr
    
    insert_data(CbCr, bin_message, k)

    return Y, CbCr


def f3_decode(Y, CbCr) -> str:
    bin_message = ''
    
    def extract_data(layer) -> str:
        imsize = layer.shape
        data = ''

        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            
                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0:
                                continue

                            data += str(abs(coef) % 2)
        return data

    bin_message += extract_data(Y) + extract_data(CbCr)

    return decode_binary_message(bin_message)


def f4_encode(Y, CbCr, msg):
        
    k = 0
    bin_message = encode_binary_message(msg)
    
    def insert_data(layer, bin_message, k) -> int:
        imsize = layer.shape
        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            if k >= len(bin_message):
                                continue

                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0:
                                continue
                            
                            if coef < 0 and ((abs(coef) % 2 == 1 and bin_message[k] == '1')\
                                 or (abs(coef) % 2 == 0 and bin_message[k] == '0')):
                                layer[i,j,c,i1,j1] = coef + 1
                            elif coef > 0 and ((abs(coef) % 2 == 1 and bin_message[k] == '0')\
                                 or (abs(coef) % 2 == 0 and bin_message[k] == '1')):
                                layer[i,j,c,i1,j1] = coef - 1
                        
                            if layer[i,j,c,i1,j1] != 0:
                                k += 1
        
        return k

    k = insert_data(Y, bin_message, k)
    
    if k >= len(bin_message):
        return Y, CbCr
    
    insert_data(CbCr, bin_message, k)

    return Y, CbCr


def f4_decode(Y, CbCr) -> str:
    bin_message = ''
    
    def extract_data(layer) -> str:
        imsize = layer.shape
        data = ''

        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            
                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0:
                                continue

                            if (coef > 0 and abs(coef) % 2 == 0) or (coef < 0 and abs(coef) % 2 == 1):
                                data += '0'
                            elif (coef > 0 and abs(coef) % 2 == 1) or (coef < 0 and abs(coef) % 2 == 0):
                                data += '1'
        return data

    bin_message += extract_data(Y) + extract_data(CbCr)

    return decode_binary_message(bin_message)


def f5_encode(Y, CbCr, msg):
            
    k = 0
    bin_message = encode_binary_message(msg)
    
    def insert_data(layer, bin_message, k) -> int:
        to_modify = []
        imsize = layer.shape
        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            if k >= len(bin_message):
                                continue

                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0:
                                continue
                            to_modify.append((i, j, c, i1, j1))

                            if len(to_modify) < 3:
                                continue
                            
                            x1, x2 = int(bin_message[k]), int(bin_message[k+1])

                            a1 = int(bin(layer[to_modify[0]])[-1])
                            a2 = int(bin(layer[to_modify[1]])[-1])
                            a3 = int(bin(layer[to_modify[2]])[-1])

                            if x1 == a1 ^ a3 and x2 == a2 ^ a3:
                                 pass
                            elif x1 != a1 ^ a3 and x2 == a2 ^ a3:
                                a1 = (a1 + 1) % 2
                            elif x1 == a1 ^ a3 and x2 != a2 ^ a3:
                                a2 = (a2 + 1) % 2
                            elif x1 != a1 ^ a3 and x2 != a2 ^ a3:
                                a3 = (a3 + 1) % 2
                            
                            layer[to_modify[0]] = int(bin(layer[to_modify[0]])[:-1] + str(a1), 2)
                            layer[to_modify[1]] = int(bin(layer[to_modify[1]])[:-1] + str(a2), 2) 
                            layer[to_modify[2]] = int(bin(layer[to_modify[2]])[:-1] + str(a3), 2)

                            if layer[to_modify[0]] == 0:
                                to_modify.pop(0)
                            elif layer[to_modify[1]] == 0:
                                to_modify.pop(1)
                            elif layer[to_modify[2]] == 0:
                                to_modify.pop(2)
                            else:
                                to_modify.clear()
                                k += 2 

        return k

    k = insert_data(Y, bin_message, k)
    
    if k >= len(bin_message):
        return Y, CbCr
    
    insert_data(CbCr, bin_message, k)

    return Y, CbCr


def f5_decode(Y, CbCr) -> str:
    bin_message = ''
    
    def extract_data(layer) -> str:
        imsize = layer.shape

        data = ''
        extract_from = []

        for i in range(imsize[0]):
            for j in range(imsize[1]):
                for c in range(imsize[2]):

                    for i1 in range(8):
                        for j1 in range(8):
                            
                            if i1 == 0 and j1 == 0:
                                continue

                            coef = layer[i,j,c,i1,j1]

                            if coef == 0:
                                continue

                            extract_from.append((i,j,c,i1,j1))

                            if len(extract_from) < 3:
                                continue

                            a1 = int(bin(layer[extract_from[0]])[-1])
                            a2 = int(bin(layer[extract_from[1]])[-1])
                            a3 = int(bin(layer[extract_from[2]])[-1])

                            data += str(a1 ^ a3) + str(a2 ^ a3)
                            
                            extract_from.clear()
                            
        return data

    bin_message += extract_data(Y) + extract_data(CbCr)

    return decode_binary_message(bin_message)