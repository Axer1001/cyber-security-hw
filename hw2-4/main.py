import numpy as np
from imageio import imread, imwrite
from encoders import lsb_encode, lsb_decode
from encoders import f3_encode, f3_decode
from encoders import f4_encode, f4_decode
from encoders import f5_encode, f5_decode
import jpeglib
from plotter import plotter
from PIL import Image
from math import log

def calc_stats(f1: str, f2: str):
    im1 = Image.open(f1)
    im2 = Image.open(f2)
    w, h = im1.width, im1.height

    m1 = im1.load()
    m2 = im2.load()

    mse = 0
    for i in range(w):
        for j in range(h):
            p1 = m1[i,j]
            p2 = m2[i,j]
            for k in range(3):
                mse += (p1[k] - p2[k]) ** 2
    mse /= (w * h)
    return mse, 10 * log((255 ** 2 / mse), 10)


def main():
    mode = int(input('Encode/decode [0/1]: '))
    method = int(input("""
Choose method:
    1. JSteg 
    2. F3
    3. F4
    4. F5
Choice [1-4]: """))
    if mode == 0:
        msg = input('Enter message: ')
        with jpeglib.JPEG("images/container.jpg") as im:
            Y_container, CbCr_container, _ = im.read_dct()

            Y_new, CbCr_new = Y_container.copy(), CbCr_container.copy() 
            
            if method == 1:
                Y_new, CbCr_new = lsb_encode(Y_new, CbCr_new, msg)
            elif method == 2:
                Y_new, CbCr_new = f3_encode(Y_new, CbCr_new, msg)
            elif method == 3:
                Y_new, CbCr_new = f4_encode(Y_new, CbCr_new, msg)
            elif method == 4:
                Y_new, CbCr_new = f5_encode(Y_new, CbCr_new, msg)

            
            im.write_dct("images/stego.jpg", Y_new, CbCr_new)

            mse, psnr = calc_stats("images/container.jpg", "images/stego.jpg")
            print(f'## STATS -- MSE: {mse}, PSNR {psnr} ##')
            
            plotter(Y_container, CbCr_container, Y_new, CbCr_new)

    elif mode == 1:
        
        with jpeglib.JPEG("images/stego.jpg") as im:
            Y, CbCr, qt = im.read_dct()
            
            if method == 1:
                msg = ''.join([c for c in lsb_decode(Y, CbCr) if c.isprintable()])
            elif method == 2:
                msg = ''.join([c for c in f3_decode(Y, CbCr) if c.isprintable()])
            elif method == 3:
                msg = ''.join([c for c in f4_decode(Y, CbCr) if c.isprintable()])
            elif method == 4:
                msg = ''.join([c for c in f5_decode(Y, CbCr) if c.isprintable()])
            print('Message:', msg)


if __name__ == '__main__':
    main()
